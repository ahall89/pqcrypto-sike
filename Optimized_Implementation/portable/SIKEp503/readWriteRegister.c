/*
 * readWriteRegister.c
 *
 *  Created on: Feb 15, 2018
 *      Author: John Ratz
 */

#include "readWriteRegister.h"

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <math.h>       // ceil
#include <ctype.h>      // isprint
#include "byteswap.h"

static bool readWrite = true;
static bool verbose   = false;

#ifndef HEXDUMP_COLS
#define HEXDUMP_COLS          8
#endif
#ifndef HEXDUMP_ABRV_LINES
#define HEXDUMP_ABRV_LINES    5
#endif

//**********************************************************************
// Name:        readRegister
//
// Purpose:     Read a single register
//
// Processing:  This will perform all of the processing necessary
//              to use the drivers to read a single register from the
//              Arria10 board.
//
// Notes:   None
//**********************************************************************
uint32_t readRegister(
    volatile void *map_base,
    uint32_t       barOffset)
{
    uint32_t read_result = 0xDEADBEEF;

    // Return if reading and writing to memory is turned off
    if (readWrite)
    {
        void *virt_addr;

        off_t target = barOffset;

        if (map_base != (void *)-1)
        {
            virt_addr   = (char *)map_base + target;
            read_result = *((uint32_t *)virt_addr);

            if (verbose)
            {
                printf("Reading from address 0x%08X, read 0x%08X\n", barOffset, read_result);
            }
        }
        else
        {
            printf("ERROR:  FAILED to Read from address 0x%08X because map_base is uninitialized\n",
                   barOffset);
        }
    }

    return read_result;
}

//**********************************************************************
// Name:        readAndPrintRegisters
//
// Purpose:     Read and print multiple registers
//
// Processing:  This will perform all of the processing necessary
//              to use the drivers to read multiple registers from the
//              Arria10 board.
//
// Notes:   None
//**********************************************************************
void readAndPrintRegisters(
    volatile void *map_base,
    uint32_t       barOffset,
    uint32_t       numWordsToRead,
    uint32_t       startAddr)
{
    uint32_t readVal = 0;
    uint32_t addr    = 0;

    printf("0x%08X: ", addr + barOffset + startAddr);
    for (addr = 0; addr < (numWordsToRead * 4); addr += 4)
    {
        // readVal = *(uint32_t *)(memMap + addr);
        readVal = readRegister(map_base, addr + barOffset);
        if (addr % 16 == 0 && addr != 0)
        {
            printf("\n0x%08X: ", addr + barOffset + startAddr);
        }
        printf("%08X ", readVal);
    }
    printf("\n");
}

//**********************************************************************
// Name:        writeRegister
//
// Purpose:     Write a single register
//
// Processing:  This will perform all of the processing necessary
//              to use the drivers to write a single register to the
//              Arria10 board.
//
// Notes:   None
//**********************************************************************
bool writeRegister(
    volatile void *map_base,
    uint32_t       barOffset,
    uint32_t       writeValue,
    bool           readBeforeWriteAndUpdate,
    bool           readVerify,
    uint32_t       mask                 // Only applicable if readBeforeWriteAndUpdate == true
    )
{
    bool     stat     = true;
    uint32_t newValue = 0;

    if (verbose)
    {
        printf("...inside writeRegister, readWrite is %i, verbose is %i, readBeforeWriteAndUpdate is %i, readVerify is %i\n",
               readWrite,
               verbose,
               readBeforeWriteAndUpdate,
               readVerify);
    }

    // Return if reading and writing to memory is turned off
    if (readWrite)
    {
        // void *map_base, *virt_addr;
        void    *virt_addr;
        uint32_t read_result;

        off_t    target = barOffset;

        if (map_base != (void *)-1)
        {
            virt_addr = (char *)map_base + target;

            if (readBeforeWriteAndUpdate)
            {
                read_result = *((uint32_t *)virt_addr);
                newValue    = (read_result & ~mask) | (writeValue & mask);
            }
            else
            {
                newValue = writeValue;
            }

            // Write to memory
            *((uint32_t *)virt_addr) = newValue;

            // Readback if specified by the readVerify param
            if (readVerify)
            {
                read_result = *((uint32_t *)virt_addr);

                if (newValue != read_result && read_result != 0xdeadbeef)
                {
                    stat = false; // write has failed
                    printf("ERROR: Write to address 0x%08X FAILED, wrote 0x%08X, read 0x%08X\n",
                           barOffset, newValue, read_result);
                }
                else if (verbose)
                {
                    printf("Writing to address 0x%08X, wrote 0x%08X, read 0x%08X\n",
                           barOffset, newValue, read_result);
                }
            }
            else if (verbose)
            {
                printf("Writing to address 0x%08X, wrote 0x%08X\n",
                       barOffset, newValue);
            }
        }
        else
        {
            printf("ERROR:  FAILED to Write to address 0x%08X because map_base is uninitialized\n",
                   barOffset);
        }
    }
    else
    {
        stat = false;
    }

    return stat;
}

void hexlify(const void *mem, unsigned int lenInBytes)
{
    uint32_t readVal        = 0;
    uint32_t addr           = 0;
    uint32_t numWordsToRead = lenInBytes / 4;

    for (addr = 0; addr < (numWordsToRead * 4); addr += 4)
    {
        readVal = *(uint32_t *)(mem + addr);
        printf("%08X", readVal);
    }
    printf("\n");
}

void hexlifyByteSwapped(const void *mem, unsigned int lenInBytes)
{
    uint32_t readVal        = 0;
    uint32_t addr           = 0;
    uint32_t numWordsToRead = lenInBytes / 4;

    for (addr = 0; addr < (numWordsToRead * 4); addr += 4)
    {
        readVal = *(uint32_t *)(mem + addr);
        printf("%08X", bswap_32(readVal));
    }
    printf("\n");
}

void hexlifyWithNewLines(const void *mem, unsigned int lenInBytes, unsigned int numLeadingSpaces, unsigned int numColumns)
{
    uint32_t     readVal        = 0;
    uint32_t     addr           = 0;
    uint32_t     numWordsToRead = lenInBytes / 4;
    unsigned int i;

    for (addr = 0; addr < (numWordsToRead * 4); addr += 4)
    {
        readVal = *(uint32_t *)(mem + addr);
        printf("%08x", readVal);

        if ((((addr + 4) * 2) % numColumns == 0) && ((addr + 4) < (numWordsToRead * 4)))
        {
            printf("\n");
            for (i = 0; i < numLeadingSpaces; i++)
            {
                printf(" ");
            }
        }
    }
    printf("\n");
}

void hexlifyByteSwappedWithNewLines(const void *mem, unsigned int lenInBytes, unsigned int numLeadingSpaces, unsigned int numColumns)
{
//    uint32_t readVal        = 0;
//    uint32_t addr           = 0;
//    uint32_t numWordsToRead = lenInBytes / 4;
//
//    for (addr = 0; addr < (numWordsToRead * 4); addr += 4)
//    {
//        readVal = *(uint32_t *)(mem + addr);
//        printf("%08x", bswap_32(readVal));
//
//        if ((((addr + 4) * 2) % numColumns == 0) && ((addr + 4) < (numWordsToRead * 4)))
//        {
//            printf("\n");
//            for (unsigned int i = 0; i < numLeadingSpaces; i++)
//            {
//                printf(" ");
//            }
//        }
//    }
//    printf("\n");
    unsigned int i;
    uint8_t      readVal = 0;
    uint8_t      addr    = 0;

    for (addr = 0; addr < lenInBytes; addr++)
    {
        readVal = *(uint8_t *)(mem + addr);
        printf("%02x", readVal);

        if ((((addr + 1) * 2) % numColumns == 0) && ((addr + 1) < lenInBytes))
        {
            printf("\n");
            for (i = 0; i < numLeadingSpaces; i++)
            {
                printf(" ");
            }
        }
    }
    printf("\n");
}

void hexdump(const void *mem, unsigned int len, bool abbreviate)
{
    unsigned int i, j;
    unsigned int totalLinesNeeded = ceil(len / (double)HEXDUMP_COLS);
    unsigned int linesProcessed   = 0;

    // Only abbreviate (if requested) when there are more than 10 lines
    if (totalLinesNeeded <= (HEXDUMP_ABRV_LINES * 2))
    {
        abbreviate = false;
    }

    for (i = 0; i < len + ((len % HEXDUMP_COLS) ? (HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++)
    {
        if (abbreviate)
        {
            if (linesProcessed == HEXDUMP_ABRV_LINES)
            {
                printf("...\n");
            }

            if (linesProcessed >= HEXDUMP_ABRV_LINES && linesProcessed < (totalLinesNeeded - HEXDUMP_ABRV_LINES))
            {
                i += (HEXDUMP_COLS - 1);
                linesProcessed++;
                continue;
            }
        }

        // Print offset
        if (i % HEXDUMP_COLS == 0)
        {
            printf("(%03u) 0x%06X: ", linesProcessed, i);
        }

        // Print hex data
        if (i < len)
        {
            printf("%02X ", 0xFF & ((char *)mem)[i]);
        }
        // End of block, just aligning for ASCII dump
        else
        {
            printf("   ");
        }

        // Print ASCII dump
        if (i % HEXDUMP_COLS == (HEXDUMP_COLS - 1))
        {
            for (j = i - (HEXDUMP_COLS - 1); j <= i; j++)
            {
                // End of block, not really printing
                if (j >= len)
                {
                    putchar(' ');
                }
                // Printable char
                else if (isprint(((char *)mem)[j]))
                {
                    putchar(0xFF & ((char *)mem)[j]);
                }
                // Other char
                else
                {
                    putchar('.');
                }
            }
            putchar('\n');
            linesProcessed++;
        }
    }
}

void hexdumpVol(const volatile void *mem, unsigned int len, bool abbreviate)
{
    unsigned int i, j;
    unsigned int totalLinesNeeded = ceil(len / (double)HEXDUMP_COLS);
    unsigned int linesProcessed   = 0;

    // Only abbreviate (if requested) when there are more than 10 lines
    if (totalLinesNeeded <= (HEXDUMP_ABRV_LINES * 2))
    {
        abbreviate = false;
    }

    for (i = 0; i < len + ((len % HEXDUMP_COLS) ? (HEXDUMP_COLS - len % HEXDUMP_COLS) : 0); i++)
    {
        if (abbreviate)
        {
            if (linesProcessed == HEXDUMP_ABRV_LINES)
            {
                printf("...\n");
            }

            if (linesProcessed >= HEXDUMP_ABRV_LINES && linesProcessed < (totalLinesNeeded - HEXDUMP_ABRV_LINES))
            {
                i += (HEXDUMP_COLS - 1);
                linesProcessed++;
                continue;
            }
        }

        // Print offset
        if (i % HEXDUMP_COLS == 0)
        {
            printf("(%03u) 0x%06X: ", linesProcessed, i);
        }

        // Print hex data
        if (i < len)
        {
            printf("%02X ", 0xFF & ((char *)mem)[i]);
        }
        // End of block, just aligning for ASCII dump
        else
        {
            printf("   ");
        }

        // Print ASCII dump
        if (i % HEXDUMP_COLS == (HEXDUMP_COLS - 1))
        {
            for (j = i - (HEXDUMP_COLS - 1); j <= i; j++)
            {
                // End of block, not really printing
                if (j >= len)
                {
                    putchar(' ');
                }
                // Printable char
                else if (isprint(((char *)mem)[j]))
                {
                    putchar(0xFF & ((char *)mem)[j]);
                }
                // Other char
                else
                {
                    putchar('.');
                }
            }
            putchar('\n');
            linesProcessed++;
        }
    }
}

//volatile void *naive_memset(volatile void *s, int c, size_t n)
//{
//    unsigned char *bytes = (unsigned char *)s;
//
//    for (size_t i = 0; i < n; i++)
//    {
//        bytes[i] = (unsigned char)c;
//    }
//
//    return s;
//}
//
//volatile void *naive_memcpy(volatile void *_dest, const volatile void *_src, size_t n)
//{
//    unsigned char *dest = (unsigned char *)_dest;
//    unsigned char *src  = (unsigned char *)_src;
//
//    for (size_t i = 0; i < n; i++)
//    {
//        dest[i] = src[i];
//    }
//
//    return dest;
//}
//
//int naive_memcmp(const volatile void *a1, const volatile void *a2, size_t n)
//{
//    unsigned char *char1 = (unsigned char *)a1;
//    unsigned char *char2 = (unsigned char *)a2;
//
//    for (size_t i = 0; i < n; i++)
//    {
//        if (char1[i] < char2[i])
//        {
//            return -1;
//        }
//        if (char1[i] > char2[i])
//        {
//            return 1;
//        }
//        if (char1[i] == 0)
//        {
//            return 0;
//        }
//    }
//
//    return 0;
//}

volatile void *naive_memset(volatile void *s, int c, size_t n)
{
    size_t    i;

    uint32_t *words  = (uint32_t *)s;

    size_t    nWords = n / (sizeof(uint32_t));

    for (i = 0; i < nWords; i++)
    {
        words[i] = (uint32_t)c;
    }

    return s;
}

volatile void *naive_memcpy(volatile void *_dest, const volatile void *_src, size_t n)
{
    size_t    i;

    uint32_t *dest   = (uint32_t *)_dest;
    uint32_t *src    = (uint32_t *)_src;

    size_t    nWords = n / (sizeof(uint32_t));

    for (i = 0; i < nWords; i++)
    {
        dest[i] = src[i];
    }

    return dest;
}

volatile void *naive_memcpy_byteswap(volatile void *_dest, const volatile void *_src, size_t n)
{
    size_t    i;

    uint32_t *dest   = (uint32_t *)_dest;
    uint32_t *src    = (uint32_t *)_src;

    size_t    nWords = n / (sizeof(uint32_t));

    for (i = 0; i < nWords; i++)
    {
        dest[i] = bswap_32(src[i]);
    }

    return dest;
}

volatile void *naive_memcpy_byteswap_and_reverse(volatile void *_dest, const volatile void *_src, size_t n)
{
    size_t    i, j;

    uint32_t *dest   = (uint32_t *)_dest;
    uint32_t *src    = (uint32_t *)_src;

    size_t    nWords = n / (sizeof(uint32_t));

    for (i = 0, j = nWords - 1; i < nWords; i++, j--)
    {
        dest[i] = bswap_32(src[j]);
    }

    return dest;
}

int naive_memcmp(const volatile void *a1, const volatile void *a2, size_t n)
{
    size_t    i;

    uint32_t *word1  = (uint32_t *)a1;
    uint32_t *word2  = (uint32_t *)a2;

    size_t    nWords = n / (sizeof(uint32_t));

    for (i = 0; i < nWords; i++)
    {
        if (word1[i] < word2[i])
        {
            return -1;
        }
        if (word1[i] > word2[i])
        {
            return 1;
        }
        if (word1[i] == 0)
        {
            return 0;
        }
    }

    return 0;
}
