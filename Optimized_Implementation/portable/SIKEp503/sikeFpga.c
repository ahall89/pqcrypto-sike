/*
 * sikeFpga.c
 *
 *  Created on: Aug 27, 2018
 *      Author: John Ratz
 */

#include <stdio.h>     // printf
#include <string.h>    // memcpy
#include <math.h>      // ceil
#include <unistd.h>
#include <inttypes.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include "sikeFpga.h"
#include "sikeFpgaParameters.h"
#include "readWriteRegister.h"
#include "memtest.h"

//static const int   MAX_FRAM_SIZE_WORDS = 65536; // 2^16
//static const int   MAX_NUM_DRAMS       = 256;
//
//// Use globals for FRAM and DRAMs
//uint32_t           framInstructions[MAX_FRAM_SIZE_WORDS];
//uint32_t           dramInArray[MAX_NUM_DRAMS][DRAM_SIZE_WORDS];

bool            useSikeInterrupts           = true;
pthread_mutex_t sikeMutex;
int             memFileDescriptor           = -1;
int             sikeInterruptFileDescriptor = -1;
int             sikeFramFileDescriptor      = -1;
volatile void  *memMap;


//**********************************************************************
// Name:        clearAllRegisters
//
// Purpose:     Clear all the SIKE FPGA Registers
//
// Processing:
//      -  Clear SCRATCH_REG register
//      -  Clear MODE_REG register
//      -  Clear I_RAM registers
//      -  Clear Q_REG register
//      -  Clear C_RAM registers
//      -  Clear SEED_RAM registers
//      -  Clear MESSAGE_RAM registers
//      -  Clear K_RAM registers
//
// Notes:   None
//**********************************************************************
void clearAllRegisters(volatile void *memMap)
{
    printf("#################################################################################\n");
    printf("    Clearing all RAM Registers\n");
    printf("#################################################################################\n\n");

    printf("\nClearing SCRATCH_REG\n");
    naive_memset(memMap + SCRATCH_REG_OFFSET, 0x0, REG_SIZE_BYTES);

    printf("\nClearing MODE_REG\n");
    naive_memset(memMap + MODE_REG_OFFSET, 0x0, REG_SIZE_BYTES);

    printf("\n\n");
}

//**********************************************************************
// Name:        clearDataRams
//
// Purpose:     Clear all of the Data Ram
//
// Processing:
//      -  Clear DRAMs
//
// Notes:   None
//**********************************************************************
void clearDataRams(volatile void *memMap)
{
//    printf("#################################################################################\n");
//    printf("    Clearing Data Ram Spaces\n");
//    printf("#################################################################################\n\n");
//
//    printf("\nClearing DRAM0\n");
    naive_memset(memMap + DRAM0_OFFSET, 0x0, TOTAL_DRAM_SIZE_BYTES);

//    printf("\n\n");
}

//**********************************************************************
// Name:        clearFunctionRam
//
// Purpose:     Clear all of the Function Ram
//
// Processing:
//      -  Clear FRAM
//
// Notes:   None
//**********************************************************************
void clearFunctionRam(volatile void *memMap)
{
//    printf("#################################################################################\n");
//    printf("    Clearing Function Ram Space\n");
//    printf("#################################################################################\n\n");
//
//    printf("\nClearing FRAM\n");
    naive_memset(memMap + FRAM_OFFSET, 0x0, FRAM_SIZE_BYTES);

//    printf("\n\n");
}

//**********************************************************************
// Name:        readAndPrintAllRegisters
//
// Purpose:     Read and print all the SIKE FPGA Registers
//
// Processing:
//      -  Read VERSION_REG register
//      -  Read STATUS_REG register
//      -  Read SCRATCH_REG register
//      -  Read MODE_REG register
//
// Notes:   None
//**********************************************************************
void readAndPrintAllRegisters(volatile void *memMap)
{
    printf("#################################################################################\n");
    printf("    Reading all Registers\n");
    printf("#################################################################################\n\n");

    printf("VERSION_REG:\n");
    readAndPrintRegisters(memMap, VERSION_REG_OFFSET, 1, START_BRIDGE_ADDR);

    printf("\nSTATUS_REG:\n");
    readAndPrintStatusRegister(memMap, NULL);

    printf("\nSCRATCH_REG:\n");
    readAndPrintRegisters(memMap, SCRATCH_REG_OFFSET, 1, START_BRIDGE_ADDR);

    printf("\nMODE_REG:\n");
    readAndPrintModeRegister(memMap, NULL);

    printf("\nDRAM0:\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\nDRAM1:\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\nDRAM2:\n");
    readAndPrintRegisters(memMap, DRAM2_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\nDRAM3:\n");
    readAndPrintRegisters(memMap, DRAM3_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\nDRAM4:\n");
    readAndPrintRegisters(memMap, DRAM4_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\nDRAM5:\n");
    readAndPrintRegisters(memMap, DRAM5_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\nFRAM:\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);

    printf("\n\n");
}

//**********************************************************************
// Name:        performDataRamTest
//
// Purpose:     Performs a memory integrity test of the Data Ram
//
// Returns:
//    true:   Data RAM is operating correctly
//    false:  Data RAM is not operating correctly
//
// Processing:
//      -  Test FRAM
//
// Notes:   None
//**********************************************************************
bool performDataRamTest(volatile void *memMap)
{
    bool retVal = true;

    printf("#################################################################################\n");
    printf("    Test Data Ram Space for Memory Integrity\n");
    printf("#################################################################################\n\n");

    printf("\nTesting DRAM0\n");
    if ((memTestDataBus(memMap, DRAM0_OFFSET) != 0)
        || (memTestAddressBus(memMap, DRAM0_OFFSET, DRAM_SIZE_BYTES) != NULL)
        || (memTestDevice(memMap, DRAM0_OFFSET, DRAM_SIZE_BYTES) != NULL))
    {
        retVal = false;
    }

    if (retVal)
    {
        printf("\nTesting DRAM1\n");
        if ((memTestDataBus(memMap, DRAM1_OFFSET) != 0)
            || (memTestAddressBus(memMap, DRAM1_OFFSET, DRAM_SIZE_BYTES) != NULL)
            || (memTestDevice(memMap, DRAM1_OFFSET, DRAM_SIZE_BYTES) != NULL))
        {
            retVal = false;
        }
    }

    if (retVal)
    {
        printf("\nTesting DRAM2\n");
        if ((memTestDataBus(memMap, DRAM2_OFFSET) != 0)
            || (memTestAddressBus(memMap, DRAM2_OFFSET, DRAM_SIZE_BYTES) != NULL)
            || (memTestDevice(memMap, DRAM2_OFFSET, DRAM_SIZE_BYTES) != NULL))
        {
            retVal = false;
        }
    }

    if (retVal)
    {
        printf("\nTesting DRAM3\n");
        if ((memTestDataBus(memMap, DRAM3_OFFSET) != 0)
            || (memTestAddressBus(memMap, DRAM3_OFFSET, DRAM_SIZE_BYTES) != NULL)
            || (memTestDevice(memMap, DRAM3_OFFSET, DRAM_SIZE_BYTES) != NULL))
        {
            retVal = false;
        }
    }

    if (retVal)
    {
        printf("\nTesting DRAM4\n");
        if ((memTestDataBus(memMap, DRAM4_OFFSET) != 0)
            || (memTestAddressBus(memMap, DRAM4_OFFSET, DRAM_SIZE_BYTES) != NULL)
            || (memTestDevice(memMap, DRAM4_OFFSET, DRAM_SIZE_BYTES) != NULL))
        {
            retVal = false;
        }
    }

    if (retVal)
    {
        printf("\nTesting DRAM5\n");
        if ((memTestDataBus(memMap, DRAM5_OFFSET) != 0)
            || (memTestAddressBus(memMap, DRAM5_OFFSET, DRAM_SIZE_BYTES) != NULL)
            || (memTestDevice(memMap, DRAM5_OFFSET, DRAM_SIZE_BYTES) != NULL))
        {
            retVal = false;
        }
    }

    printf("\n\n");

    return retVal;
}

//**********************************************************************
// Name:        performFunctionRamTest
//
// Purpose:     Performs a memory integrity test of the Function Ram
//
// Returns:
//    true:   Function RAM is operating correctly
//    false:  Function RAM is not operating correctly
//
// Processing:
//      -  Test FRAM
//
// Notes:   None
//**********************************************************************
bool performFunctionRamTest(volatile void *memMap)
{
    bool retVal = true;

    printf("#################################################################################\n");
    printf("    Test Function Ram Space for Memory Integrity\n");
    printf("#################################################################################\n\n");

    printf("\nTesting FRAM\n");
    if ((memTestDataBus(memMap, FRAM_OFFSET) != 0)
        || (memTestAddressBus(memMap, FRAM_OFFSET, FRAM_SIZE_BYTES) != NULL)
        || (memTestDevice(memMap, FRAM_OFFSET, FRAM_SIZE_BYTES) != NULL))
    {
        retVal = false;
    }

    printf("\n\n");

    return retVal;
}

//**********************************************************************
// Name:        performScratchTest
//
// Purpose:     Verifies that the Scratch Register is operating correctly
//
// Returns:
//    true:   Scratch Register is operating correctly
//    false:  Scratch Register is not operating correctly
//
// Processing:
//     -  Do a read/write test in the Scratch Register with 0xDEADBEEF
//     -  Do a read/write test in the Scratch Register with 0x00C0FFEE
//
// Notes:   None
//**********************************************************************
bool performScratchTest(volatile void *memMap)
{
    bool retVal = true;

    printf("#################################################################################\n");
    printf("    Test Scratch Register for Memory Integrity\n");
    printf("#################################################################################\n\n");

    printf("\nTesting SCRATCH_REG\n");

    if ((memTestDataBus(memMap, SCRATCH_REG_OFFSET) != 0))
    {
        retVal = false;
    }

    return retVal;
}

//**********************************************************************
// Name:        performExample1
//
// Purpose:     Performs the steps necessary to perform the example
//              laid out in the ICD, i.e. XQ = (XP+ZP)^2 * C24 *(XP-ZP)^2
//
// Processing:
//    -  TBD
//
// Notes:   None
//**********************************************************************
void performExample1(volatile void *memMap,
                     int            sikeFileDescriptor,
                     const uint32_t PrimeToUse_Val,
                     uint32_t      *XQ_md,
                     uint32_t      *ZQ_md)
{
    printf("\n#################################################################################\n");
    printf("    Performing Example 1 on the FPGA\n");
    printf("#################################################################################\n\n");

    // Load Prime Vale to use in Mode register = PrimeToUse_Val
    readAndPrintModeRegister(memMap, "before updating Prime Val");
    printf("Setting bits in Mode Register at PRIME_VALUE_MASK (0x%08X) with PrimeToUse_Val=0x%X...\n", PRIME_VALUE_MASK, PrimeToUse_Val);
    writeRegister(memMap, MODE_REG_OFFSET, PrimeToUse_Val, true, true, PRIME_VALUE_MASK);
    readAndPrintModeRegister(memMap, "after updating Prime Val");

    // Load FRAM = FRAM_EXAMPLE1_TEST_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_EXAMPLE1_TEST_VEC...\n");
    naive_memcpy(memMap + FRAM_OFFSET, FRAM_EXAMPLE1_TEST_VEC, sizeof(FRAM_EXAMPLE1_TEST_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM0 = DRAM0_EXAMPLE1_TEST_VEC
    printf("\nDRAM0_OFFSET (before copy from DRAM0_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM0 with DRAM0_EXAMPLE1_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM0_OFFSET, DRAM0_EXAMPLE1_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM0_OFFSET (after copy from DRAM0_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM1 = DRAM1_EXAMPLE1_TEST_VEC
    printf("\nDRAM1_OFFSET (before copy from DRAM1_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM1 with DRAM1_EXAMPLE1_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM1_OFFSET, DRAM1_EXAMPLE1_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM1_OFFSET (after copy from DRAM1_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM2 = DRAM2_EXAMPLE1_TEST_VEC
    printf("\nDRAM2_OFFSET (before copy from DRAM2_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM2_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM2 with DRAM2_EXAMPLE1_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM2_OFFSET, DRAM2_EXAMPLE1_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM2_OFFSET (after copy from DRAM2_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM2_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM3 = DRAM3_EXAMPLE1_TEST_VEC
    printf("\nDRAM3_OFFSET (before copy from DRAM3_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM3_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM3 with DRAM3_EXAMPLE1_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM3_OFFSET, DRAM3_EXAMPLE1_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM3_OFFSET (after copy from DRAM3_EXAMPLE1_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM3_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Reset the START_FRAM_ADDR_REG_OFFSET
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (before reset):\n");
    readAndFramOffsetRegister(memMap, "before resetting to 0");
    printf("Setting Start FRAM Offset Register to 0x%X...\n", 0);
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, 0, false, true, 0);
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (after reset):\n");
    readAndFramOffsetRegister(memMap, "after resetting to 0");

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeFileDescriptor);

    // Read DRAM4 (768 bits, or 6 reads of 32-bits)
    printf("\nXQ is in DRAM4:\n");
    readAndPrintRegisters(memMap, DRAM4_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Store DRAM4 in XQ_md
    printf("\nCopying DRAM4 to XQ_md...\n");
    naive_memcpy(XQ_md, memMap + DRAM4_OFFSET, DRAM_SIZE_BYTES);

    printf("\nXQ_md (after copy from DRAM4):\n");
    readAndPrintRegisters(XQ_md, 0x0, DRAM_SIZE_WORDS, 0x0);
    printf("\nXQ_md (in format from ICD):\n0x");
    for (int i = DRAM_SIZE_WORDS - 1; i >= 0; i--)
    {
        printf("%08X", XQ_md[i]);
    }
    printf("\n");
    printf("hexlified:  ");
    hexlify(XQ_md, DRAM_SIZE_BYTES);
    hexdump(XQ_md, DRAM_SIZE_BYTES, false);

    // Read DRAM5 (768 bits, or 6 reads of 32-bits)
    printf("\nZQ is in DRAM5:\n");
    readAndPrintRegisters(memMap, DRAM5_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Store DRAM5 in ZQ_md
    printf("\nCopying DRAM5 to ZQ_md...\n");
    naive_memcpy(ZQ_md, memMap + DRAM5_OFFSET, DRAM_SIZE_BYTES);

    printf("\nZQ_md (after copy from DRAM5):\n");
    readAndPrintRegisters(ZQ_md, 0x0, DRAM_SIZE_WORDS, 0x0);
    printf("\nZQ_md (in format from ICD):\n0x");
    for (int i = DRAM_SIZE_WORDS - 1; i >= 0; i--)
    {
        printf("%08X", ZQ_md[i]);
    }
    printf("\n");
    printf("hexlified:  ");
    hexlify(ZQ_md, DRAM_SIZE_BYTES);
    hexdump(ZQ_md, DRAM_SIZE_BYTES, false);

    printf("\n\n");
}

//**********************************************************************
// Name:        performMultiplicationTest
//
// Purpose:     Performs the steps necessary to perform a simple multiplication
//              test that was provided by John Wade
//
// Processing:
//    -  TBD
//
// Notes:   None
//**********************************************************************
void performMultiplicationTest(volatile void *memMap,
                               int            sikeFileDescriptor,
                               const uint32_t PrimeToUse_Val,
                               uint32_t      *ZQ_md)
{
    printf("\n#################################################################################\n");
    printf("    Performing Multiplication Test on the FPGA\n");
    printf("#################################################################################\n\n");

    // Load Prime Value to use in Mode register = PrimeToUse_Val
    readAndPrintModeRegister(memMap, "before updating Prime Val");
    printf("Setting bits in Mode Register at PRIME_VALUE_MASK (0x%08X) with PrimeToUse_Val=0x%X...\n", PRIME_VALUE_MASK, PrimeToUse_Val);
    writeRegister(memMap, MODE_REG_OFFSET, PrimeToUse_Val, true, true, PRIME_VALUE_MASK);
    readAndPrintModeRegister(memMap, "after updating Prime Val");

    // Load FRAM = FRAM_MULTIPLICATION_TEST_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_MULTIPLICATION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_MULTIPLICATION_TEST_VEC...\n");
    naive_memcpy(memMap + FRAM_OFFSET, FRAM_MULTIPLICATION_TEST_VEC, sizeof(FRAM_MULTIPLICATION_TEST_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_MULTIPLICATION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM0 = DRAM0_MULTIPLICATION_TEST_VEC
    printf("\nDRAM0_OFFSET (before copy from DRAM0_MULTIPLICATION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM0 with DRAM0_MULTIPLICATION_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM0_OFFSET, DRAM0_MULTIPLICATION_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM0_OFFSET (after copy from DRAM0_MULTIPLICATION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM1 = DRAM1_MULTIPLICATION_TEST_VEC
    printf("\nDRAM1_OFFSET (before copy from DRAM1_MULTIPLICATION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM1 with DRAM1_MULTIPLICATION_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM1_OFFSET, DRAM1_MULTIPLICATION_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM1_OFFSET (after copy from DRAM1_MULTIPLICATION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Reset the START_FRAM_ADDR_REG_OFFSET
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (before reset):\n");
    readAndFramOffsetRegister(memMap, "before resetting to 0");
    printf("Setting Start FRAM Offset Register to 0x%X...\n", 0);
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, 0, false, true, 0);
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (after reset):\n");
    readAndFramOffsetRegister(memMap, "after resetting to 0");

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeFileDescriptor);

    // Read DRAM1 (768 bits, or 6 reads of 32-bits)
    printf("\nZQ is in DRAM1:\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Store DRAM1 in ZQ_md
    printf("\nCopying DRAM1 to ZQ_md...\n");
    naive_memcpy(ZQ_md, memMap + DRAM1_OFFSET, DRAM_SIZE_BYTES);

    printf("\nZQ_md (after copy from DRAM1):\n");
    readAndPrintRegisters(ZQ_md, 0x0, DRAM_SIZE_WORDS, 0x0);
    printf("\nZQ_md (in format from ICD):\n0x");
    for (int i = DRAM_SIZE_WORDS - 1; i >= 0; i--)
    {
        printf("%08X", ZQ_md[i]);
    }
    printf("\n");
    printf("hexlified:  ");
    hexlify(ZQ_md, DRAM_SIZE_BYTES);
    hexdump(ZQ_md, DRAM_SIZE_BYTES, false);

    printf("\n\n");
}

//**********************************************************************
// Name:        performAdditionTest
//
// Purpose:     Performs the steps necessary to perform a simple addition
//              test that was provided by John Wade
//
// Processing:
//    -  TBD
//
// Notes:   None
//**********************************************************************
void performAdditionTest(volatile void *memMap,
                         int            sikeFileDescriptor,
                         const uint32_t PrimeToUse_Val,
                         uint32_t      *ZQ_md)
{
    printf("\n#################################################################################\n");
    printf("    Performing Addition Test on the FPGA\n");
    printf("#################################################################################\n\n");

    // Load Prime Value to use in Mode register = PrimeToUse_Val
    readAndPrintModeRegister(memMap, "before updating Prime Val");
    printf("Setting bits in Mode Register at PRIME_VALUE_MASK (0x%08X) with PrimeToUse_Val=0x%X...\n", PRIME_VALUE_MASK, PrimeToUse_Val);
    writeRegister(memMap, MODE_REG_OFFSET, PrimeToUse_Val, true, true, PRIME_VALUE_MASK);
    readAndPrintModeRegister(memMap, "after updating Prime Val");

    // Load FRAM = FRAM_ADDITION_TEST_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_ADDITION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_ADDITION_TEST_VEC...\n");
    naive_memcpy(memMap + FRAM_OFFSET, FRAM_ADDITION_TEST_VEC, sizeof(FRAM_ADDITION_TEST_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_ADDITION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM0 = DRAM0_ADDITION_TEST_VEC
    printf("\nDRAM0_OFFSET (before copy from DRAM0_ADDITION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM0 with DRAM0_ADDITION_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM0_OFFSET, DRAM0_ADDITION_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM0_OFFSET (after copy from DRAM0_ADDITION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Reset the START_FRAM_ADDR_REG_OFFSET
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (before reset):\n");
    readAndFramOffsetRegister(memMap, "before resetting to 0");
    printf("Setting Start FRAM Offset Register to 0x%X...\n", 0);
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, 0, false, true, 0);
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (after reset):\n");
    readAndFramOffsetRegister(memMap, "after resetting to 0");

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeFileDescriptor);

    // Read DRAM1 (768 bits, or 6 reads of 32-bits)
    printf("\nZQ is in DRAM1:\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Store DRAM1 in ZQ_md
    printf("\nCopying DRAM1 to ZQ_md...\n");
    naive_memcpy(ZQ_md, memMap + DRAM1_OFFSET, DRAM_SIZE_BYTES);

    printf("\nZQ_md (after copy from DRAM1):\n");
    readAndPrintRegisters(ZQ_md, 0x0, DRAM_SIZE_WORDS, 0x0);
    printf("\nZQ_md (in format from ICD):\n0x");
    for (int i = DRAM_SIZE_WORDS - 1; i >= 0; i--)
    {
        printf("%08X", ZQ_md[i]);
    }
    printf("\n");
    printf("hexlified:  ");
    hexlify(ZQ_md, DRAM_SIZE_BYTES);
    hexdump(ZQ_md, DRAM_SIZE_BYTES, false);

    printf("\n\n");
}

//**********************************************************************
// Name:        performSubtractionTest
//
// Purpose:     Performs the steps necessary to perform a simple subtraction
//              test that was provided by John Wade
//
// Processing:
//    -  TBD
//
// Notes:   None
//**********************************************************************
void performSubtractionTest(volatile void *memMap,
                            int            sikeFileDescriptor,
                            const uint32_t PrimeToUse_Val,
                            uint32_t      *ZQ_md)
{
    printf("\n#################################################################################\n");
    printf("    Performing Subtraction Test on the FPGA\n");
    printf("#################################################################################\n\n");

    // Load Prime Value to use in Mode register = PrimeToUse_Val
    readAndPrintModeRegister(memMap, "before updating Prime Val");
    printf("Setting bits in Mode Register at PRIME_VALUE_MASK (0x%08X) with PrimeToUse_Val=0x%X...\n", PRIME_VALUE_MASK, PrimeToUse_Val);
    writeRegister(memMap, MODE_REG_OFFSET, PrimeToUse_Val, true, true, PRIME_VALUE_MASK);
    readAndPrintModeRegister(memMap, "after updating Prime Val");

    // Load FRAM = FRAM_SUBTRACTION_TEST_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_SUBTRACTION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_SUBTRACTION_TEST_VEC...\n");
    naive_memcpy(memMap + FRAM_OFFSET, FRAM_SUBTRACTION_TEST_VEC, sizeof(FRAM_SUBTRACTION_TEST_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_SUBTRACTION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM1 = DRAM1_SUBTRACTION_TEST_VEC
    printf("\nDRAM1_OFFSET (before copy from DRAM1_SUBTRACTION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM1 with DRAM1_SUBTRACTION_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM1_OFFSET, DRAM1_SUBTRACTION_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM1_OFFSET (after copy from DRAM1_SUBTRACTION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM1_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Load DRAM2 = DRAM2_SUBTRACTION_TEST_VEC
    printf("\nDRAM2_OFFSET (before copy from DRAM2_SUBTRACTION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM2_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
    printf("Loading DRAM2 with DRAM2_SUBTRACTION_TEST_VEC...\n");
    naive_memcpy(memMap + DRAM2_OFFSET, DRAM2_SUBTRACTION_TEST_VEC, DRAM_SIZE_BYTES);
    printf("DRAM2_OFFSET (after copy from DRAM2_SUBTRACTION_TEST_VEC):\n");
    readAndPrintRegisters(memMap, DRAM2_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Reset the START_FRAM_ADDR_REG_OFFSET
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (before reset):\n");
    readAndFramOffsetRegister(memMap, "before resetting to 0");
    printf("Setting Start FRAM Offset Register to 0x%X...\n", 0);
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, 0, false, true, 0);
    printf("\nSTART_FRAM_ADDR_REG_OFFSET (after reset):\n");
    readAndFramOffsetRegister(memMap, "after resetting to 0");

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeFileDescriptor);

    // Read DRAM0 (768 bits, or 6 reads of 32-bits)
    printf("\nZQ is in DRAM0:\n");
    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);

    // Store DRAM0 in ZQ_md
    printf("\nCopying DRAM0 to ZQ_md...\n");
    naive_memcpy(ZQ_md, memMap + DRAM0_OFFSET, DRAM_SIZE_BYTES);

    printf("\nZQ_md (after copy from DRAM0):\n");
    readAndPrintRegisters(ZQ_md, 0x0, DRAM_SIZE_WORDS, 0x0);
    printf("\nZQ_md (in format from ICD):\n0x");
    for (int i = DRAM_SIZE_WORDS - 1; i >= 0; i--)
    {
        printf("%08X", ZQ_md[i]);
    }
    printf("\n");
    printf("hexlified:  ");
    hexlify(ZQ_md, DRAM_SIZE_BYTES);
    hexdump(ZQ_md, DRAM_SIZE_BYTES, false);

    printf("\n\n");
}

bool performFuzzTest(volatile void *memMap,
                     int            sikeFileDescriptor,
                     uint32_t       primeToUse,
                     int            test_num,
                     uint32_t       framInstructions[],
                     uint32_t       dramInArray[][DRAM_SIZE_WORDS],
                     uint32_t       dramOutArray[][DRAM_SIZE_WORDS],
                     int            fram_size,
                     int            dram_in_size,
                     int            dram_out_size)
{
    bool retVal = true;
    int  i;

    printf("\n#################################################################################\n");
    printf("    Performing Fuzz Test %d on the FPGA\n", test_num);
    printf("#################################################################################\n\n");

    printf("Found test=%d with fram_size=%d, dram_in_size=%d, dram_out_size=%d\n\tFRAM = ",
           test_num,
           fram_size,
           dram_in_size,
           dram_out_size);
    for (int j = 0; j < fram_size; ++j)
    {
        printf("0x%08x ", framInstructions[j]);
    }
    printf("\n\tDRAM_IN:\n");
    for (int j = 0; j < dram_in_size; ++j)
    {
        printf("\t\tOffset = %d\n\t\t\t", j);
        for (int k = 0; k < DRAM_SIZE_WORDS; ++k)
        {
            printf("0x%08X ", dramInArray[j][k]);
        }
        printf("\n");
    }
    printf("\n\tDRAM_OUT:\n");
    for (int j = 0; j < dram_out_size; ++j)
    {
        printf("\t\tOffset = %d\n\t\t\t", j);
        for (int k = 0; k < DRAM_SIZE_WORDS; ++k)
        {
            printf("0x%08X ", dramOutArray[j][k]);
        }
        printf("\n");
    }

    // Load Prime Value to use in Mode register = primeToUse
//    readAndPrintModeRegister(memMap, "before updating Prime Val");
//    printf("Setting bits in Mode Register at PRIME_VALUE_MASK (0x%08X) with PrimeToUse_Val=0x%X...\n", PRIME_VALUE_MASK, PrimeToUse_Val);
    writeRegister(memMap, MODE_REG_OFFSET, primeToUse, true, true, PRIME_VALUE_MASK);
//    readAndPrintModeRegister(memMap, "after updating Prime Val");

    // Clear out the Data RAMs
//    clearDataRams(memMap);

    // Clear out the Function RAM
//    clearFunctionRam(memMap);

    // Load FRAM = framInstructions
    naive_memcpy(memMap + FRAM_OFFSET, framInstructions, fram_size * sizeof(uint32_t));

#if 0
    // Verify that the FRAM matches what we just wrote out
    if (0 != naive_memcmp(memMap + FRAM_OFFSET, framInstructions, fram_size * sizeof(uint32_t)))
    {
        uint32_t *framResult;
        framResult = calloc(fram_size, sizeof(uint32_t));
        naive_memcpy(framResult, memMap + FRAM_OFFSET, fram_size * sizeof(uint32_t));

        printf("The FRAM DID NOT MATCH what was copied in\n");
        retVal     = false;

        printf("\tExpected:  ");
        for (int j = 0; j < fram_size; ++j)
        {
            printf("0x%08X ", framInstructions[j]);
        }
        printf("\n\tFound:     ");
        for (int j = 0; j < fram_size; ++j)
        {
            printf("0x%08X ", framResult[j]);
        }
        printf("\n");

        free(framResult);
    }
#endif

    // Load the Input DRAMs
    for (i = 0; i < dram_in_size; ++i)
    {
        naive_memcpy(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), dramInArray[i], DRAM_SIZE_BYTES);

#if 0
        // Verify that the DRAM matches what we just wrote out
        if (0 != naive_memcmp(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), dramInArray[i], DRAM_SIZE_BYTES))
        {
            uint32_t dramResult[DRAM_SIZE_WORDS] = { 0 };
            naive_memcpy(dramResult, memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), DRAM_SIZE_BYTES);

            printf("The DRAM DID NOT MATCH what was copied in\n");
            retVal = false;

            printf("\tExpected:  ");
            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
            {
                printf("0x%08X ", dramInArray[i][j]);
            }
            printf("\n\tFound:     ");
            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
            {
                printf("0x%08X ", dramResult[j]);
            }
            printf("\nSTOPPING TEST...\n");
            exit(EXIT_FAILURE);
        }
#endif
    }

    // Reset the START_FRAM_ADDR_REG_OFFSET
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, 0, false, true, 0);

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeFileDescriptor);

    // Compare the Output DRAMs to expected results
    for (i = 0; i < dram_out_size; ++i)
    {
        if (0 != naive_memcmp(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), dramOutArray[i], DRAM_SIZE_BYTES))
        {
            uint32_t dramResult[DRAM_SIZE_WORDS] = { 0 };
            naive_memcpy(dramResult, memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), DRAM_SIZE_BYTES);

            printf("The DRAM at index=%d DID NOT MATCH the expected results\n", i);
            retVal = false;

#if 0
            {
                uint32_t dramResult_v2[DRAM_SIZE_WORDS] = { 0 };
                printf("Sleeping 1 second more to see if that makes a difference in the DRAM settling to the right answer...\n");
                sleep(1);
                naive_memcpy(dramResult_v2, memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), DRAM_SIZE_BYTES);
                if (0 == memcmp(dramResult, dramResult_v2, DRAM_SIZE_BYTES))
                {
                    printf("Nope, the DRAM is still the same.\n");
                }
                else
                {
                    printf("YES, the DRAM changed in the last second! It is now:   \n");
                    for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
                    {
                        printf("0x%08X ", dramResult_v2[j]);
                    }
                    printf("\n");
                }
            }
#endif

            printf("\tExpected:  ");
            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
            {
                printf("0x%08X ", dramOutArray[i][j]);
            }
            printf("\n\tFound:     ");
            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
            {
                printf("0x%08X ", dramResult[j]);
            }
            printf("\n");
        }
    }

    printf("\n\nFuzz test %d %s!\n\n", test_num, (retVal ? "PASSED" : "FAILED"));

//    // Read DRAM0 (768 bits, or 6 reads of 32-bits)
//    printf("\nZQ is in DRAM0:\n");
//    readAndPrintRegisters(memMap, DRAM0_OFFSET, DRAM_SIZE_WORDS, START_BRIDGE_ADDR);
//
//    // Store DRAM0 in ZQ_md
//    printf("\nCopying DRAM0 to ZQ_md...\n");
//    naive_memcpy(ZQ_md, memMap + DRAM0_OFFSET, DRAM_SIZE_BYTES);
//
//    printf("\nZQ_md (after copy from DRAM0):\n");
//    readAndPrintRegisters(ZQ_md, 0x0, DRAM_SIZE_WORDS, 0x0);
//    printf("\nZQ_md (in format from ICD):\n0x");
//    for (int i = DRAM_SIZE_WORDS - 1; i >= 0; i--)
//    {
//        printf("%08X", ZQ_md[i]);
//    }
//    printf("\n");
//    printf("hexlified:  ");
//    hexlify(ZQ_md, DRAM_SIZE_BYTES);
//    hexdump(ZQ_md, DRAM_SIZE_BYTES, false);
//
//    printf("\n\n");

    return retVal;
}

bool performBuildingBlock(volatile void *memMap,
                          int            sikeFileDescriptor,
                          uint32_t       framInstructions[],
                          uint32_t       dramInArray[][DRAM_SIZE_WORDS],
                          int            fram_size,
                          int            dram_in_size)
{
    bool retVal = true;
    int  i;

    printf("\n#################################################################################\n");
    printf("    Performing Building Block on the FPGA\n");
    printf("#################################################################################\n\n");

    printf("Found building block with fram_size=%d, dram_in_size=%d\n\tFRAM = ",
           fram_size,
           dram_in_size);
    for (int j = 0; j < fram_size; ++j)
    {
        printf("0x%08x ", framInstructions[j]);
    }
    printf("\n\tDRAM_IN:\n");
    for (int j = 0; j < dram_in_size; ++j)
    {
        printf("\t\tOffset = %d\n\t\t\t", j);
        for (int k = 0; k < DRAM_SIZE_WORDS; ++k)
        {
            printf("0x%08X ", dramInArray[j][k]);
        }
        printf("\n");
    }

    // Load FRAM = framInstructions
    naive_memcpy(memMap + FRAM_OFFSET, framInstructions, fram_size * sizeof(uint32_t));

#if 0
    // Verify that the FRAM matches what we just wrote out
    if (0 != naive_memcmp(memMap + FRAM_OFFSET, framInstructions, fram_size * sizeof(uint32_t)))
    {
        uint32_t *framResult;
        framResult = calloc(fram_size, sizeof(uint32_t));
        naive_memcpy(framResult, memMap + FRAM_OFFSET, fram_size * sizeof(uint32_t));

        printf("The FRAM DID NOT MATCH what was copied in\n");
        retVal     = false;

        printf("\tExpected:  ");
        for (int j = 0; j < fram_size; ++j)
        {
            printf("0x%08X ", framInstructions[j]);
        }
        printf("\n\tFound:     ");
        for (int j = 0; j < fram_size; ++j)
        {
            printf("0x%08X ", framResult[j]);
        }
        printf("\n");

        free(framResult);
    }
#endif

    // Load the Input DRAMs
    for (i = 0; i < dram_in_size; ++i)
    {
        naive_memcpy(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), dramInArray[i], DRAM_SIZE_BYTES);

#if 0
        // Verify that the DRAM matches what we just wrote out
        if (0 != naive_memcmp(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), dramInArray[i], DRAM_SIZE_BYTES))
        {
            uint32_t dramResult[DRAM_SIZE_WORDS] = { 0 };
            naive_memcpy(dramResult, memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), DRAM_SIZE_BYTES);

            printf("The DRAM DID NOT MATCH what was copied in\n");
            retVal = false;

            printf("\tExpected:  ");
            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
            {
                printf("0x%08X ", dramInArray[i][j]);
            }
            printf("\n\tFound:     ");
            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
            {
                printf("0x%08X ", dramResult[j]);
            }
            printf("\nSTOPPING TEST...\n");
            exit(EXIT_FAILURE);
        }
#endif
    }

    // Reset the START_FRAM_ADDR_REG_OFFSET
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, 0, false, true, 0);

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeFileDescriptor);

//    // Compare the Output DRAMs to expected results
//    for (i = 0; i < dram_out_size; ++i)
//    {
//        if (0 != naive_memcmp(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), dramOutArray[i], DRAM_SIZE_BYTES))
//        {
//            uint32_t dramResult[DRAM_SIZE_WORDS] = { 0 };
//            naive_memcpy(dramResult, memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), DRAM_SIZE_BYTES);
//
//            printf("The DRAM at index=%d DID NOT MATCH the expected results\n", i);
//            retVal = false;
//
//#if 0
//            {
//                uint32_t dramResult_v2[DRAM_SIZE_WORDS] = { 0 };
//                printf("Sleeping 1 second more to see if that makes a difference in the DRAM settling to the right answer...\n");
//                sleep(1);
//                naive_memcpy(dramResult_v2, memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * i), DRAM_SIZE_BYTES);
//                if (0 == memcmp(dramResult, dramResult_v2, DRAM_SIZE_BYTES))
//                {
//                    printf("Nope, the DRAM is still the same.\n");
//                }
//                else
//                {
//                    printf("YES, the DRAM changed in the last second! It is now:   \n");
//                    for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
//                    {
//                        printf("0x%08X ", dramResult_v2[j]);
//                    }
//                    printf("\n");
//                }
//            }
//#endif
//
//            printf("\tExpected:  ");
//            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
//            {
//                printf("0x%08X ", dramOutArray[i][j]);
//            }
//            printf("\n\tFound:     ");
//            for (int j = 0; j < DRAM_SIZE_WORDS; ++j)
//            {
//                printf("0x%08X ", dramResult[j]);
//            }
//            printf("\n");
//        }
//    }

    return retVal;
}


//**********************************************************************
// Name:        startFunctionAndWaitForInterrupt
//
// Purpose:     Starts the Function and waits for an interrupt
//              to indicate that it has completed.
//
// Processing:
//    -  Send START signal
//    -  Wait for interrupt to fire indicating that the Function has finished
//
// Notes:   None
//**********************************************************************
void startFunctionAndWaitForInterrupt(volatile void *memMap, int sikeFileDescriptor)
{
//    long long      timeDiffUsec = 0;
//    struct timeval interval;
//    struct timeval startEarlier;
//    struct timeval startLater;
    uint32_t readAttempts = 1;

//    gettimeofday(&startEarlier, NULL);

    // Send START
//    printf("\nSending START signal...\n");
    writeRegister(memMap, START_FUNCTION_REG_OFFSET, 1, false, false, 0); // Write only register, can't read back value

    if (useSikeInterrupts)
    {
        bool    foundOpsDoneInterrupt = false;
        char    inputBuffer[10]       = "\0\0\0\0\0\0\0\0\0\0";
        ssize_t readLength;

        // The below code waits for the interrupt signal
        printf("\nWaiting for SIKE Operations Done Interrupt:\n");

        /* Keep trying to read a byte from SIKE driver.  This read will block until
         * the driver receives an interrupt from the SIKE.  The driver will return an
         * 'S' byte character for a SIKE Operations Done Interrupt detected
         */
        while (!foundOpsDoneInterrupt)
        {
            /* Read a byte from the SIKE driver. */
            readLength = read(sikeFileDescriptor, inputBuffer, 1);

            if (readLength > 0)
            {
                switch (inputBuffer[0])
                {
                case 'S':
                    printf("SIKE Operations Done Interrupt detected!\n");
                    foundOpsDoneInterrupt = true;
                    break;
                }
            }
            else
            {
                printf("readLength = %u\n", (uint32_t)readLength);
            }
        }

        readAndPrintStatusRegister(memMap, "after SIKE Operations Done Interrupt detected");
    }
    else
    {
        // Poll the status register until done signal is detected
        uint32_t              readVal   = readRegister(memMap, STATUS_REG_OFFSET);
        statusRegisterStruct *statusReg = (statusRegisterStruct *)&readVal;

        while (!statusReg->status_done && readAttempts < MAX_READ_ATTEMPTS)
        {
            readVal = readRegister(memMap, STATUS_REG_OFFSET);
            readAttempts++;
        }

        if (readAttempts >= MAX_READ_ATTEMPTS)
        {
            printf("Operation FAILED to complete within %u read attempts!; Status = %u\n",
                   MAX_READ_ATTEMPTS,
                   readVal);
        }

        writeRegister(memMap, CLEAR_IRQ_REG_OFFSET, CLEAR_INTERRUPTS_MASK, true, false, CLEAR_INTERRUPTS_MASK); // Write only register, can't read back value
    }

//    gettimeofday(&startLater, NULL);
//    timeDiffUsec = timeval_diff(&interval, &startLater, &startEarlier);
//    printf("Time spent in startFunctionAndWaitForInterrupt (readAttempts = %u):  %f seconds; %f msec; %lld usec\n",
//           readAttempts,
//           timeDiffUsec / 1000.0 / 1000.0,
//           timeDiffUsec / 1000.0,
//           timeDiffUsec);
//    fflush(stdout);
}

void readAndPrintStatusRegister(volatile void *memMap, char *msg)
{
    uint32_t              readVal   = readRegister(memMap, STATUS_REG_OFFSET);
    statusRegisterStruct *statusReg = (statusRegisterStruct *)&readVal;

    if (NULL == msg)
    {
        printf("Status register (0x%08X):\n", readVal);
    }
    else
    {
        printf("Status register (0x%08X) %s:\n", readVal, msg);
    }
    printf("    status_done                    = %s\n", (statusReg->status_done ? "true" : "false"));
    printf("    status_busy                    = %s\n", (statusReg->status_busy ? "true" : "false"));
    printf("    status_unused                  = 0x%X\n", statusReg->status_unused);
}

void readAndPrintModeRegister(volatile void *memMap, char *msg)
{
    uint32_t            readVal = readRegister(memMap, MODE_REG_OFFSET);
    modeRegisterStruct *modeReg = (modeRegisterStruct *)&readVal;

    if (NULL == msg)
    {
        printf("Mode register (0x%08X):\n", readVal);
    }
    else
    {
        printf("Mode register (0x%08X) %s:\n", readVal, msg);
    }
    printf("    mode_prime            = 0x%X - %s\n", modeReg->mode_prime, (modeReg->mode_prime ? "PRIME_503" : "PRIME_751"));
    printf("    mode_unused           = 0x%X\n", modeReg->mode_unused);
}

void readAndFramOffsetRegister(volatile void *memMap, char *msg)
{
    uint32_t readVal = readRegister(memMap, START_FRAM_ADDR_REG_OFFSET);

    if (NULL == msg)
    {
        printf("FRAM Offset register (0x%08X):\n", readVal);
    }
    else
    {
        printf("FRAM Offset register (0x%08X) %s:\n", readVal, msg);
    }
}

//**********************************************************************
// Name:        timeval_diff
//
// Purpose:     Utility function to determine the difference between
//              two timing points.
//
// Processing:
//    a. Check for NULL input
//    b. Subtract end from start and store in passed in timeval struct
//    c. Account for second overflow
//    d. Return difference in usec
//
// Notes:   None
//**********************************************************************
long long timeval_diff(
    struct timeval *difference,
    struct timeval *end_time,
    struct timeval *start_time)
{
    struct timeval temp_diff;

    /* Check for NULL input */
    if (difference == NULL)
    {
        difference = &temp_diff;
    }

    /* Subtract end from start and store in passed in timeval struct */
    difference->tv_sec  = end_time->tv_sec - start_time->tv_sec;
    difference->tv_usec = end_time->tv_usec - start_time->tv_usec;

    /* Account for second overflow */
    /* Using while instead of if below makes the code slightly more robust. */
    while (difference->tv_usec < 0)
    {
        difference->tv_usec += 1000000;
        difference->tv_sec  -= 1;
    }

    /* Return difference in usec */
    return 1000000LL * difference->tv_sec +
           difference->tv_usec;
}

//**********************************************************************
// Name:        loadFramFromFile
//
// Purpose:     Performs the steps necessary to parse the FRAM instructions
//              for each basic block from a config file, and then load
//              them into memory appropriately.
//
// Processing:
//    -  TBD
//
// Notes:   None
//**********************************************************************
bool loadFramFromFile(volatile void *memMap,
                      int            sikeFileDescriptor)
{
    bool retVal = true;

    printf("\n#################################################################################\n");
    printf("    Loading SIKE Operations into the FRAM on the FPGA\n");
    printf("#################################################################################\n\n");

    // JMR ToDo - Write code that reads in and parses an FRAM config file
    // For right now, just write out the xDBL FRAM

//    // Load FRAM = FRAM_xDBL_VEC
//    printf("\nFRAM_OFFSET (before copy from FRAM_xDBL_VEC) memMap = %p:\n", memMap);
//    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);
//    printf("Loading FRAM with FRAM_xDBL_VEC...\n");
//    naive_memcpy(memMap + FRAM_OFFSET, FRAM_xDBL_VEC, sizeof(FRAM_xDBL_VEC));
//    printf("FRAM_OFFSET (after copy from FRAM_xDBL_VEC):\n");
//    readAndPrintRegisters(memMap, FRAM_OFFSET, FRAM_PRINT_SIZE_WORDS, START_BRIDGE_ADDR);

//    static const uint32_t FRAM_eval_3_isog_OFFSET = 0;
//    static const uint32_t FRAM_eval_4_isog_OFFSET = 132;
//    static const uint32_t FRAM__OFFSET  = 316;
//    static const uint32_t FRAM__OFFSET  = 462;
//    static const uint32_t FRAM__OFFSET     = 538;
//    static const uint32_t FRAM_xDBL_OFFSET        = 758;
//    static const uint32_t FRAM__OFFSET        = 890;
//    static const uint32_t FRAM_INVALID_OFFSET     = 0xFFFFFFFF;

    // Load FRAM = FRAM_eval_3_isog_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_eval_3_isog_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_eval_3_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_eval_3_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_eval_3_isog_VEC at word offset %u (byte offset 0x%" PRIX32 ")...\n",
           FRAM_eval_3_isog_OFFSET,
           FRAM_eval_3_isog_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_eval_3_isog_OFFSET * sizeof(uint32_t), FRAM_eval_3_isog_VEC, sizeof(FRAM_eval_3_isog_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_eval_3_isog_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_eval_3_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_eval_3_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);

    // Load FRAM = FRAM_eval_4_isog_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_eval_4_isog_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_eval_4_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_eval_4_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_eval_4_isog_VEC at word offset %u (byte offset 0x%X)...\n",
           FRAM_eval_4_isog_OFFSET,
           FRAM_eval_4_isog_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_eval_4_isog_OFFSET * sizeof(uint32_t), FRAM_eval_4_isog_VEC, sizeof(FRAM_eval_4_isog_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_eval_4_isog_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_eval_4_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_eval_4_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);

    // Load FRAM = FRAM_get_3_isog_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_get_3_isog_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_get_3_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_get_3_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_get_3_isog_VEC at word offset %u (byte offset 0x%X)...\n",
           FRAM_get_3_isog_OFFSET,
           FRAM_get_3_isog_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_get_3_isog_OFFSET * sizeof(uint32_t), FRAM_get_3_isog_VEC, sizeof(FRAM_get_3_isog_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_get_3_isog_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_get_3_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_get_3_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);

    // Load FRAM = FRAM_get_4_isog_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_get_4_isog_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_get_4_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_get_4_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_get_4_isog_VEC at word offset %u (byte offset 0x%X)...\n",
           FRAM_get_4_isog_OFFSET,
           FRAM_get_4_isog_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_get_4_isog_OFFSET * sizeof(uint32_t), FRAM_get_4_isog_VEC, sizeof(FRAM_get_4_isog_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_get_4_isog_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_get_4_isog_OFFSET * sizeof(uint32_t), sizeof(FRAM_get_4_isog_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);

    // Load FRAM = FRAM_xDBLADD_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_xDBLADD_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_xDBLADD_OFFSET * sizeof(uint32_t), sizeof(FRAM_xDBLADD_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_xDBLADD_VEC at word offset %u (byte offset 0x%X)...\n",
           FRAM_xDBLADD_OFFSET,
           FRAM_xDBLADD_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_xDBLADD_OFFSET * sizeof(uint32_t), FRAM_xDBLADD_VEC, sizeof(FRAM_xDBLADD_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_xDBLADD_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_xDBLADD_OFFSET * sizeof(uint32_t), sizeof(FRAM_xDBLADD_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);

    // Load FRAM = FRAM_xDBL_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_xDBL_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_xDBL_OFFSET * sizeof(uint32_t), sizeof(FRAM_xDBL_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_xDBL_VEC at word offset %u (byte offset 0x%X)...\n",
           FRAM_xDBL_OFFSET,
           FRAM_xDBL_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_xDBL_OFFSET * sizeof(uint32_t), FRAM_xDBL_VEC, sizeof(FRAM_xDBL_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_xDBL_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_xDBL_OFFSET * sizeof(uint32_t), sizeof(FRAM_xDBL_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);

    // Load FRAM = FRAM_xTPL_VEC
    printf("\nFRAM_OFFSET (before copy from FRAM_xTPL_VEC) memMap = %p:\n", memMap);
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_xTPL_OFFSET * sizeof(uint32_t), sizeof(FRAM_xTPL_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);
    printf("Loading FRAM with FRAM_xTPL_VEC at word offset %u (byte offset 0x%X)...\n",
           FRAM_xTPL_OFFSET,
           FRAM_xTPL_OFFSET * (uint32_t)sizeof(uint32_t));
    naive_memcpy(memMap + FRAM_OFFSET + FRAM_xTPL_OFFSET * sizeof(uint32_t), FRAM_xTPL_VEC, sizeof(FRAM_xTPL_VEC));
    printf("FRAM_OFFSET (after copy from FRAM_xTPL_VEC):\n");
    readAndPrintRegisters(memMap, FRAM_OFFSET + FRAM_xTPL_OFFSET * sizeof(uint32_t), sizeof(FRAM_xTPL_VEC) / sizeof(uint32_t), START_BRIDGE_ADDR);


    if (-1 == sikeFramFileDescriptor)
    {
        printf("loadFramFromFile:  sikeFramFileDescriptor is not initialized!\n");
//        retVal = false;
    }
    else
    {
        printf("TBD - need to write code to actually parse the file using regular expressions and the PCRE library...\n");
    }

    return retVal;
}

int initSikeFPGA(void)
{
    int returnVal = 0;       // Assume success

    // Init mutex
    if (pthread_mutex_init(&sikeMutex, NULL) != 0)
    {
        printf("\n mutex init failed for sikeMutex\n");

        return 1;
    }

    size_t len = STOP_BRIDGE_ADDR - START_BRIDGE_ADDR;

    /* Open memory file */
    memFileDescriptor = open("/dev/mem", O_RDWR | O_SYNC);
    if (-1 == memFileDescriptor)
    {
        fprintf(stderr, "Failed to open '%s': %s\n", "/dev/mem", strerror(errno));
        goto err_mmap;
    }

    /* Perform the memory map */
    memMap = mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_SHARED, memFileDescriptor, START_BRIDGE_ADDR);
    if (MAP_FAILED == memMap)
    {
        perror("Failed to mmmap");
        goto err_mmap;
    }

    /* Open the SIKE driver handle read only. */
    sikeInterruptFileDescriptor = open("/dev/sike0", O_RDONLY);

    if (-1 == sikeInterruptFileDescriptor)
    {
        printf("Couldn't open /dev/sike0! Setting useSikeInterrupts = false\n");
        useSikeInterrupts = false;
    }
    else
    {
        printf("Found /dev/sike0! Will attempt to use the FPGA (vice software) to perform SIKE Operations...\n");
    }

    /* Open the SIKE FRAM file */
    sikeFramFileDescriptor = open("./sikeFramConfig.txt", O_RDONLY);
    if (-1 == sikeFramFileDescriptor)
    {
        printf("Couldn't open ./sikeFramConfig.txt!\n");
    }
    else
    {
        printf("Found ./sikeFramConfig.txt! Will attempt to parse the FRAM instructions for SIKE Operations...\n");
    }

    return returnVal;

 err_mmap:
    returnVal = 1;

    if (memFileDescriptor)
    {
        /* Close the memory file */
        (void)close(memFileDescriptor);
        memFileDescriptor = -1;
    }

    if (sikeInterruptFileDescriptor)
    {
        /* Close the SIKE interrupt file */
        (void)close(sikeInterruptFileDescriptor);
        sikeInterruptFileDescriptor = -1;
    }

    if (sikeFramFileDescriptor)
    {
        /* Close the SIKE FRAM file */
        (void)close(sikeFramFileDescriptor);
        sikeFramFileDescriptor = -1;
    }

    return returnVal;
}
