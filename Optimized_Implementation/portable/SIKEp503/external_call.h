#include "P503_internal.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>

typedef enum CallType
{
    xDBL_basic_block,
    get_4_isog_basic_block,
    eval_4_isog_basic_block,
    xTPL_basic_block,
    get_3_isog_basic_block,
    eval_3_isog_basic_block,
    inv_3_way_basic_block,
    get_A_basic_block,
    j_inv_basic_block,
    xDBLADD_basic_block,
    LADDER3PT_basic_block,
    MAX_basic_block
} CallType_t;

typedef struct dram_input_arg
{
    int           offset;
    const felm_t *value;
} dram_input_arg;

typedef struct dram_output_arg
{
    int     offset;
    felm_t *value;
} dram_output_arg;

const char *CallType2Str(const CallType_t callType);

uint32_t CallType2Offset(const CallType_t callType);

int cmp_felm_t(const felm_t op1, const felm_t op2);

int cmp_f2elm_t(const f2elm_t op1, const f2elm_t op2);

int cmp_point_proj_t(const point_proj_t op1, const point_proj_t op2);

void copy_felm_t(felm_t dest, const felm_t src);

void copy_f2elm_t(f2elm_t dest, const f2elm_t src);

void copy_point_proj_t(point_proj_t dest, const point_proj_t src);

void print_felm_t(FILE *out, const felm_t x);

void print_dfelm_t(FILE *out, const dfelm_t x);

void print_f2elm_t(FILE *out, const f2elm_t x);

void print_point_proj_t(FILE *out, const point_proj_t);

void read_felm_t(FILE *in, felm_t x);

void read_f2elm_t(FILE *in, f2elm_t x);

void read_point_proj_t(FILE *in, point_proj_t x);

void perform_external_call(CallType_t             callType,
                           unsigned               num_inputs,
                           unsigned               num_outputs,
                           const dram_input_arg  *input_vars,
                           const dram_output_arg *output_vars);
