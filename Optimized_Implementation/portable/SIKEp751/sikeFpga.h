/*
 * sikeFpga.h
 *
 *  Created on: Nov 07, 2018
 *      Author: John Ratz
 */

#ifndef SIKE_FPGA_H_
#define SIKE_FPGA_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include <sys/time.h>
#include <pthread.h>

#include "sikeFpgaParameters.h"

void clearAllRegisters(volatile void *memMap);
void clearDataRams(volatile void *memMap);
void clearFunctionRam(volatile void *memMap);

void readAndPrintAllRegisters(volatile void *memMap);

bool performDataRamTest(volatile void *memMap);
bool performFunctionRamTest(volatile void *memMap);
bool performScratchTest(volatile void *memMap);

void performExample1(volatile void *memMap,
                     int            sikeFileDescriptor,
                     const uint32_t PrimeToUse_Val,
                     uint32_t      *XQ_md,
                     uint32_t      *ZQ_md);


void performMultiplicationTest(volatile void *memMap,
                               int            sikeFileDescriptor,
                               const uint32_t PrimeToUse_Val,
                               uint32_t      *ZQ_md);

void performAdditionTest(volatile void *memMap,
                         int            sikeFileDescriptor,
                         const uint32_t PrimeToUse_Val,
                         uint32_t      *ZQ_md);

void performSubtractionTest(volatile void *memMap,
                            int            sikeFileDescriptor,
                            const uint32_t PrimeToUse_Val,
                            uint32_t      *ZQ_md);

bool performFuzzTest(volatile void *memMap,
                     int            sikeFileDescriptor,
                     uint32_t       primeToUse,
                     int            test_num,
                     uint32_t       framInstructions[],
                     uint32_t       dramInArray[][DRAM_SIZE_WORDS],
                     uint32_t       dramOutArray[][DRAM_SIZE_WORDS],
                     int            fram_size,
                     int            dram_in_size,
                     int            dram_out_size);

void startFunctionAndWaitForInterrupt(volatile void *memMap, int sikeFileDescriptor);

void readAndPrintStatusRegister(volatile void *memMap, char *msg);
void readAndPrintModeRegister(volatile void *memMap, char *msg);
void readAndFramOffsetRegister(volatile void *memMap, char *msg);

long long timeval_diff(
    struct timeval *difference,
    struct timeval *end_time,
    struct timeval *start_time);

extern bool            useSikeInterrupts;
extern pthread_mutex_t sikeMutex;
extern int             memFileDescriptor;
extern int             sikeInterruptFileDescriptor;
extern int             sikeFramFileDescriptor;
extern volatile void  *memMap;

bool loadFramFromFile(volatile void *memMap,
                      int            sikeFileDescriptor);

int initSikeFPGA(void);

#ifdef __cplusplus
}
#endif

#endif /* SIKE_FPGA_H_ */
