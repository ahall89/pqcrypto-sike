#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <stdint.h>
#include <limits.h>
#include <inttypes.h>
#include "external_call.h"
#include "sikeFpga.h"
#include "sikeFpgaParameters.h"
#include "readWriteRegister.h"

#define MAX_FILE_PATH    1024

#define VAR_NAME_HELPER(name)     #name
#define VAR_NAME(x)               VAR_NAME_HELPER(x)

#define CHECK_CALL_TYPE_STR(x)    case (x): return VAR_NAME(x)

const char *CallType2Str(const CallType_t callType)
{
    switch (callType)
    {
        CHECK_CALL_TYPE_STR(xDBL_basic_block);
        CHECK_CALL_TYPE_STR(get_4_isog_basic_block);
        CHECK_CALL_TYPE_STR(eval_4_isog_basic_block);
        CHECK_CALL_TYPE_STR(xTPL_basic_block);
        CHECK_CALL_TYPE_STR(get_3_isog_basic_block);
        CHECK_CALL_TYPE_STR(eval_3_isog_basic_block);
        CHECK_CALL_TYPE_STR(inv_3_way_basic_block);
        CHECK_CALL_TYPE_STR(get_A_basic_block);
        CHECK_CALL_TYPE_STR(j_inv_basic_block);
        CHECK_CALL_TYPE_STR(xDBLADD_basic_block);
        CHECK_CALL_TYPE_STR(LADDER3PT_basic_block);

    default:

        return "Invalid Call Type";

        break;
    }
}

uint32_t CallType2Offset(const CallType_t callType)
{
    // ToDo - Need to figure out how to read in the FRAM instructions and offsets from a file; this is just temporary
    uint32_t basicBlockOffset = FRAM_INVALID_OFFSET;

    switch (callType)
    {
    case eval_3_isog_basic_block:
        basicBlockOffset = FRAM_eval_3_isog_OFFSET;
        break;

    case eval_4_isog_basic_block:
        basicBlockOffset = FRAM_eval_4_isog_OFFSET;
        break;

    case get_3_isog_basic_block:
        basicBlockOffset = FRAM_get_3_isog_OFFSET;
        break;

    case get_4_isog_basic_block:
        basicBlockOffset = FRAM_get_4_isog_OFFSET;
        break;

    case xDBLADD_basic_block:
        basicBlockOffset = FRAM_xDBLADD_OFFSET;
        break;

    case xDBL_basic_block:
        basicBlockOffset = FRAM_xDBL_OFFSET;
        break;

    case xTPL_basic_block:
        basicBlockOffset = FRAM_xTPL_OFFSET;
        break;

    case inv_3_way_basic_block:
    case get_A_basic_block:
    case j_inv_basic_block:
    case LADDER3PT_basic_block:
        printf("CallType2Offset:  ERROR - %s is unsupported on FPGA; Setting offset to 0x%08X\n",
               CallType2Str(callType),
               basicBlockOffset);
        break;

    default:
        printf("Invalid Call Type 0x%X provided to perform_external_call function; Setting offset to 0x%08X\n",
               callType,
               basicBlockOffset);
        break;
    }

    return basicBlockOffset;
}

int cmp_felm_t(const felm_t op1, const felm_t op2)
{
    return memcmp(op1, op2, NWORDS_FIELD * sizeof(digit_t));
}

int cmp_f2elm_t(const f2elm_t op1, const f2elm_t op2)
{
    int x, y;

    x = cmp_felm_t(op1[0], op2[0]);
    y = cmp_felm_t(op1[1], op2[1]);
    if (0 == x)
    {
        return y;
    }
    else
    {
        return x;
    }
}

int cmp_point_proj_t(const point_proj_t op1, const point_proj_t op2)
{
    int x, y;

    x = cmp_f2elm_t(op1->X, op2->X);
    y = cmp_f2elm_t(op1->Z, op2->Z);
    if (0 == x)
    {
        return y;
    }
    else
    {
        return x;
    }
}

void copy_felm_t(felm_t dest, const felm_t src)
{
    memcpy(dest, src, NWORDS_FIELD * sizeof(digit_t));
}

void copy_f2elm_t(f2elm_t dest, const f2elm_t src)
{
    copy_felm_t(dest[0], src[0]);
    copy_felm_t(dest[1], src[1]);
}

void copy_point_proj_t(point_proj_t dest, const point_proj_t src)
{
    copy_f2elm_t(dest->X, src->X);
    copy_f2elm_t(dest->Z, src->Z);
}

void print_felm_t(FILE *out, const felm_t x)
{
    for (size_t i = NWORDS_FIELD; i > 0; i--)
    {
#if RADIX == 32
        fprintf(out, "%08" PRIx32, x[i - 1]);   // %08x
#elif RADIX == 64
        fprintf(out, "%016" PRIx64, x[i - 1]);  // %016llx
#endif
    }
}

void print_dfelm_t(FILE *out, const dfelm_t x)
{
    for (size_t i = 2 * NWORDS_FIELD; i > 0; i--)
    {
#if RADIX == 32
        fprintf(out, "%08" PRIx32, x[i - 1]);   // %08x
#elif RADIX == 64
        fprintf(out, "%016" PRIx64, x[i - 1]);  // %016llx
#endif
    }
}

void print_f2elm_t(FILE *out, const f2elm_t x)
{
    print_felm_t(out, x[0]);
    fprintf(out, " ");
    print_felm_t(out, x[1]);
}

void print_point_proj_t(FILE *out, const point_proj_t x)
{
    print_f2elm_t(out, x->X);
    fprintf(out, "\n");
    print_f2elm_t(out, x->Z);
}

void read_felm_t(FILE *in, felm_t x)
{
    size_t i, j;
    char   felm_str[NCHARS_FIELD + 1];
    char   felm_str_copy[NCHARS_FIELD + 1];

    // Fill felm_str_copy with zeros
    for (i = 0; i < NCHARS_FIELD; i++)
    {
        felm_str_copy[i] = '0';
    }
    // Terminate the array
    felm_str_copy[NCHARS_FIELD] = '\0';

    // Read the element in
    fscanf(in, "%s", felm_str);
//    printf("Scanned in felm_str      is %s which contains %u characters\n", felm_str, (unsigned)strlen(felm_str));

    // Pad with zeros
    strcpy(felm_str_copy + (NCHARS_FIELD - strlen(felm_str)), felm_str);
    // printf("Scanned in felm_str_copy is %s which contains %u characters\n", felm_str_copy, (unsigned)strlen(felm_str_copy));

    // Scan the element in
    for (i = NWORDS_FIELD, j = 0; i > 0; i--, j++)
    {
#if RADIX == 32
        sscanf(felm_str_copy + (j * 8), "%08" SCNx32, x + i - 1);    // %8x
#elif RADIX == 64
        sscanf(felm_str_copy + (j * 16), "%016" SCNx64, x + i - 1);  // %16llx
#endif
    }
//    }
}

void read_f2elm_t(FILE *in, f2elm_t x)
{
    read_felm_t(in, x[0]);
    read_felm_t(in, x[1]);
}

void read_point_proj_t(FILE *in, point_proj_t x)
{
    read_f2elm_t(in, x->X);
    read_f2elm_t(in, x->Z);
}

void perform_basic_block_on_FPGA(uint32_t               basicBlockOffset,
                                 unsigned               num_inputs,
                                 unsigned               num_outputs,
                                 const dram_input_arg  *input_vars,
                                 const dram_output_arg *output_vars)
{
    unsigned i, j;

    // Load DRAM0 = DRAM_ZERO_VEC
    naive_memcpy(memMap + DRAM0_OFFSET, DRAM_ZERO_VEC, DRAM_SIZE_BYTES);

    // Load the input variables starting at DRAM2
    for (i = 0, j = 0; i < num_inputs; i++, j += 2)
    {
        naive_memcpy(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * (input_vars[i].offset + 0)), input_vars[i].value[0], NBYTES_FIELD);
        naive_memcpy(memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * (input_vars[i].offset + 1)), input_vars[i].value[1], NBYTES_FIELD);
    }

    // Set the START_FRAM_ADDR_REG_OFFSET to the passed in offset
    writeRegister(memMap, START_FRAM_ADDR_REG_OFFSET, basicBlockOffset, false, true, 0);

    // Send Start Function signal
    startFunctionAndWaitForInterrupt(memMap, sikeInterruptFileDescriptor);

    // Read the output variables starting where the inputs left off, i.e. do not modify j
    for (i = 0; i < num_outputs; i++, j += 2)
    {
        naive_memcpy(output_vars[i].value[0], memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * (output_vars[i].offset + 0)), NBYTES_FIELD);
        naive_memcpy(output_vars[i].value[1], memMap + DRAM0_OFFSET + (DRAM_OFFSET_SIZE_BYTES * (output_vars[i].offset + 1)), NBYTES_FIELD);
    }
}

void perform_external_call(CallType_t             callType,
                           unsigned               num_inputs,
                           unsigned               num_outputs,
                           const dram_input_arg  *input_vars,
                           const dram_output_arg *output_vars)
{
    static bool printOnceInitialized = false;
    static bool printOnce[MAX_basic_block];

    if (!printOnceInitialized)
    {
        for (int i = 0; i < MAX_basic_block; ++i)
        {
            printOnce[i] = true;
        }
        printOnceInitialized = true;
    }

#if defined(PERFORM_CALLS_IN_PYTHON) || defined(PERFORM_TEST_IN_PYTHON)
    const char *routine = CallType2Str(callType);

    // Read out variables for python
    FILE       *regfile;
    char        path[MAX_FILE_PATH];
    char        cmd[MAX_FILE_PATH];

    if (getcwd(path, sizeof(path)) != NULL)
    {
        strcat(path, "/regfile.txt");
        strcpy(cmd, "cd ~/pysogenies && /usr/bin/python handler.py ");
        strcat(cmd, path);
    }
    else
    {
        exit(-1);
    }

    // Open the input/output file
    regfile = fopen("regfile.txt", "w+");
    if (!regfile)
    {
        printf("Failed to open register file\n");

        return;
    }
    fprintf(regfile, "%s ", routine);

    // Read in the inputs
    for (size_t i = 0; i < num_inputs; i++)
    {
        print_f2elm_t(regfile, (input_vars[i].value));
        fprintf(regfile, " ");
    }

    fprintf(regfile, "\n");
    // fflush(regfile);
    fclose(regfile);

    // Run the block via Python
    system(cmd);

    // Reset the file pointer back to the beginning
    // fseek(regfile, 0, SEEK_SET);
    regfile = fopen("regfile.txt", "r");
    if (!regfile)
    {
        printf("Failed to open register file\n");

        return;
    }

    rewind(regfile);

    // Read the values computed by Python back into output_vars
    for (size_t i = 0; i < num_outputs; i++)
    {
        read_f2elm_t(regfile, output_vars[i].value);
    }
    fclose(regfile);
#endif /*defined(PERFORM_CALLS_IN_PYTHON)*/
#if defined(PERFORM_CALLS_ON_FPGA) || defined(PERFORM_TEST_ON_FPGA)
    // ToDo - Perform call on the FPGA
    uint32_t basicBlockOffset = 0;
    switch (callType)
    {
    case eval_3_isog_basic_block:
    case eval_4_isog_basic_block:
    case get_3_isog_basic_block:
    case get_4_isog_basic_block:
    case xDBLADD_basic_block:
    case xDBL_basic_block:
    case xTPL_basic_block:
        if (printOnce[callType])
        {
            printf("Received a new call to Perform %s on the FPGA\n", CallType2Str(callType));
            printOnce[callType] = false;
        }

        // ToDo - Get actual offset here based on the Call Type
        basicBlockOffset = CallType2Offset(callType);
        perform_basic_block_on_FPGA(basicBlockOffset,
                                    num_inputs,
                                    num_outputs,
                                    input_vars,
                                    output_vars);
        break;

    case inv_3_way_basic_block:
    case get_A_basic_block:
    case j_inv_basic_block:
    case LADDER3PT_basic_block:
        printf("ERROR - %s is unsupported on FPGA\n", CallType2Str(callType));
        break;

    default:
        printf("Invalid Call Type provided to perform_external_call function\n");
        break;
    }
#endif /*defined(PERFORM_CALLS_ON_FPGA)*/
}
