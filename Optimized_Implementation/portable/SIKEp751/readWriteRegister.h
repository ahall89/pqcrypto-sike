/*
 * readWriteRegister.h
 *
 *  Created on: Feb 15, 2018
 *      Author: John Ratz
 */

#ifndef READWRITEREGISTER_H_
#define READWRITEREGISTER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

//**********************************************************************
// Name:        readRegister
//
// Purpose:     Read a single register
//
// Processing:  This will perform all of the processing necessary
//              to use the drivers to read a single register from the
//              Arria10 board.
//
// Notes:   None
//**********************************************************************
uint32_t readRegister(
    volatile void *map_base,
    uint32_t       barOffset);

//**********************************************************************
// Name:        readAndPrintRegisters
//
// Purpose:     Read and print multiple registers
//
// Processing:  This will perform all of the processing necessary
//              to use the drivers to read multiple registers from the
//              Arria10 board.
//
// Notes:   None
//**********************************************************************
void readAndPrintRegisters(
    volatile void *map_base,
    uint32_t       barOffset,
    uint32_t       numWordsToRead,
    uint32_t       startAddr);

//**********************************************************************
// Name:        writeRegister
//
// Purpose:     Write a single register
//
// Processing:  This will perform all of the processing necessary
//              to use the drivers to write a single register to the
//              Arria10 board.
//
// Notes:   None
//**********************************************************************
bool writeRegister(
    volatile void *map_base,
    uint32_t       barOffset,
    uint32_t       writeValue,
    bool           readBeforeWriteAndUpdate,
    bool           readVerify,
    uint32_t       mask                 // Only applicable if readBeforeWriteAndUpdate == true
    );

void hexlify(const void *mem, unsigned int lenInBytes);

void hexlifyByteSwapped(const void *mem, unsigned int lenInBytes);

void hexlifyWithNewLines(const void *mem, unsigned int lenInBytes, unsigned int numLeadingSpaces, unsigned int numColumns);

void hexlifyByteSwappedWithNewLines(const void *mem, unsigned int lenInBytes, unsigned int numLeadingSpaces, unsigned int numColumns);

void hexdump(const void *mem, unsigned int len, bool abbreviate);

void hexdumpVol(const volatile void *mem, unsigned int len, bool abbreviate);

volatile void *naive_memset(volatile void *s, int c, size_t n);

volatile void *naive_memcpy(volatile void *_dest, const volatile void *_src, size_t n);

volatile void *naive_memcpy_byteswap(volatile void *_dest, const volatile void *_src, size_t n);

volatile void *naive_memcpy_byteswap_and_reverse(volatile void *_dest, const volatile void *_src, size_t n);

int naive_memcmp(const volatile void *a1, const volatile void *a2, size_t n);

#ifdef __cplusplus
}
#endif

#endif /* READWRITEREGISTER_H_ */
