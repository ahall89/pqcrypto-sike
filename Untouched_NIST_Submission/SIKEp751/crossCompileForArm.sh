#! /bin/bash

ARCH=ARM
CALL=native			# Options are python, native, or fpga
TESTER=fpga			# Options are python or fpga
#TEST=xDBL,xDBLADD	# Options are EVAL_3, EVAL_4, GET_3, GET_4, xDBLADD, xDBL, xTPL (note that they can be concatenated, e.g. TEST=EVAL_3,EVAL_4
#TEST=xDBL

CROSS_COMPILE=~/gcc-linaro-arm-linux-gnueabihf-4.8-2014.04_linux/bin/arm-linux-gnueabihf-


make clean
make ARCH=${ARCH} CROSS_COMPILE=${CROSS_COMPILE} CALL=${CALL} TESTER=${TESTER} TEST=${TEST}

#
# EOF - crossCompileForArm.sh
